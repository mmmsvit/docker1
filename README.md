###docker 1: php create log file with datetime inside Docker
sh run_docker1.sh

###docker 1: php create log file with datetime inside Host System
sh run_docker2.sh


Try this:
# build Dockefile and up services
clear && docker-compose build && docker-compose up

# run only one service (web)
clear && docker-compose run web

# get bash inside docker
docker-compose run web /bin/sh
