# download from Dock.Hub iamge
FROM php:7.2.7-alpine as web

# do some variables there
ARG APP_PATH=/app/
ARG FILE_NAME=file1.php
ENV FILE_NAME ${FILE_NAME}

# set current dir
WORKDIR $APP_PATH

# copy file1.php to image
COPY $FILE_NAME ./

RUN mkdir /var/log/web


# and run file1.php (only once, on the build)
# RUN php $FILE_NAME && cat /var/log/log.txt

# or every time on docker run
# ENTRYPOINT php $FILE_NAME

# Docker ARG only accept at build!!!, use ENV receive hack
# CMD php ${FILE_NAME} && less /var/log/web/log.txt && /bin/sh
# ENTRYPOINT php ${FILE_NAME}
CMD php ${FILE_NAME}

# execute command on docker run
# CMD ["/bin/sh"]
